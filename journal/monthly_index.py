"""Month index generation module."""
import calendar


def generate_index(month, year, filename):
    """Generates index for a given month and writes to the given file."""
    month_index = month_to_index(month)
    content = content_generator(month_index, year)
    with open(filename, 'w', encoding="utf-8") as file:
        file.write(content)


def content_generator(month, year):
    """Generate journal content for the given month."""
    content = ""
    month_name = calendar.month_name[month].lower()
    days = calendar.monthrange(year, month)[1]
    for day in range(days):
        content += "\n- [{d}](/{y}-{m}-{d})".format(d=day +
                                                    1, m=month_name, y=year)
    return content


def month_to_index(month):
    """Convert given month to its integer, based on calendar module."""
    for index, month_str in enumerate(list(calendar.month_abbr)):
        if month.lower() == month_str.lower() or \
                month.lower() == calendar.month_name[index].lower():
            return index
    return 0
