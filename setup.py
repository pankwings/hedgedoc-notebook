from setuptools import setup
from cli import version

setup(
    name="hedgedoc-notebook",
    version=version.VERSION,
    packages=["cli", "journal", "tests"],
    entry_points="""
        [console_scripts]
        hedgedoc-notebook=cli.__main__:cli
    """
)
