"""Journal related functionality tests."""
import os
import unittest
from journal import monthly_index

MARCH = """
- [1](/2022-march-1)
- [2](/2022-march-2)
- [3](/2022-march-3)
- [4](/2022-march-4)
- [5](/2022-march-5)
- [6](/2022-march-6)
- [7](/2022-march-7)
- [8](/2022-march-8)
- [9](/2022-march-9)
- [10](/2022-march-10)
- [11](/2022-march-11)
- [12](/2022-march-12)
- [13](/2022-march-13)
- [14](/2022-march-14)
- [15](/2022-march-15)
- [16](/2022-march-16)
- [17](/2022-march-17)
- [18](/2022-march-18)
- [19](/2022-march-19)
- [20](/2022-march-20)
- [21](/2022-march-21)
- [22](/2022-march-22)
- [23](/2022-march-23)
- [24](/2022-march-24)
- [25](/2022-march-25)
- [26](/2022-march-26)
- [27](/2022-march-27)
- [28](/2022-march-28)
- [29](/2022-march-29)
- [30](/2022-march-30)
- [31](/2022-march-31)"""


class TestContentGenerator(unittest.TestCase):
    """Test content generator."""

    def test_content_generator(self):
        """Tests the generated content for a month."""
        result = monthly_index.content_generator(3, 2022)
        self.assertEqual(result, MARCH)


class TestMonthToIndex(unittest.TestCase):
    """Test month to index."""

    def test_month_to_index(self):
        """Tests the month to index conversion for various inputs."""
        cases = {
            "Jan": 1,
            "Feb": 2,
            "Dec": 12,
            "Nov": 11,
            "Aug": 8,
            "mar": 3,
            "apr": 4,
            "April": 4,
            "september": 9,
        }
        for month, index in cases.items():
            result = monthly_index.month_to_index(month)
            self.assertEqual(result, index)


class TestGenerateIndex(unittest.TestCase):
    """Test generate index."""

    def test_generate_index(self):
        """Tests the overall index generation result."""
        testfile = "jan-index.md"
        monthly_index.generate_index("jan", 2022, testfile)
        with open(testfile, encoding="utf-8") as file:
            content = file.read()
            self.assertGreater(
                len(content), 0, msg="expected file to not be empty")
            os.remove(testfile)
