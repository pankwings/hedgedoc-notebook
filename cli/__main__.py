"""CLI main."""
import click
from journal import monthly_index
from .version import VERSION


@click.group()
@click.version_option(version=VERSION)
def cli():
    """hedgedoc-notebook CLI"""


@click.command()
@click.argument("month")
@click.argument("year")
@click.argument("file")
def journal_index_month(month, year, file):
    """Create journal index for a month."""
    click.echo("Generating month index...")
    monthly_index.generate_index(month, int(year), file)
    click.echo(f"Wrote the index in {file}")


cli.add_command(journal_index_month)

if __name__ == "__main__":
    cli()
